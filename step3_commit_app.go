package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"jira-summary-fix/lib"
	"net/http"
	"os"
)

var client = &http.Client{}

func main() {
	runCommit()
}

func runCommit() {
	csvfile, err := os.Open("step2_out.csv")
	lib.CheckError("Couldn't open the csv file", err)
	r := csv.NewReader(csvfile)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		lib.CheckError("", err)
		ticket := record[0]
		summary := record[1]
		sendRequest(ticket, summary)
	}
}

func sendRequest(key string, summary string) {
	type Data = map[string]string
	content := Data{
		"summary": summary,
	}
	postBody, err := json.Marshal(map[string]Data{
		"fields": content,
	})
	lib.CheckError("Marshall error %v", err)

	url := fmt.Sprintf("https://setelnow.atlassian.net/rest/api/2/issue/%s", key)
	responseBody := bytes.NewBuffer(postBody)
	fmt.Printf("%v\n", url)
	req, err := http.NewRequest("PUT", url, responseBody)
	lib.CheckError("reqs error %v", err)
	req.Header.Add("Content-Type", "application/json")

	config := lib.GetConfig()
	req.SetBasicAuth(config.Cred.Email, config.Cred.Token)

	resp, err := client.Do(req)
	lib.CheckError("response error %v", err)
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	lib.CheckError("body error %v", err)
}
