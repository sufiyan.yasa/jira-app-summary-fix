package main

import (
	"encoding/csv"
	"io"
	"jira-summary-fix/lib"
	"os"
	"regexp"
)

func main() {
	validEntries := matchAndReplace("step1_out.csv")
	lib.SaveToCSV("step2_out.csv", validEntries)
}

func matchAndReplace(filename string) [][]string {
	output := [][]string{}
	csvfile, err := os.Open(filename)
	lib.CheckError("Couldn't open the csv file", err)
	r := csv.NewReader(csvfile)
	isAndroidTicket := regexp.MustCompile(`(^APP\s?:\s?(A)\s?:\s?)`)
	isiOSTicket := regexp.MustCompile(`(^APP\s?:\s?(I)\s?:\s?)`)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		lib.CheckError("", err)
		summary := record[1]
		if isAndroidTicket.MatchString(summary) {
			fixed := isAndroidTicket.ReplaceAll([]byte(summary), []byte("APP: A: "))
			output = append(output, []string{record[0], string(fixed)})
		}
		if isiOSTicket.MatchString(summary) {
			fixed := isiOSTicket.ReplaceAll([]byte(summary), []byte("APP: I: "))
			output = append(output, []string{record[0], string(fixed)})
		}
	}
	return output

}
