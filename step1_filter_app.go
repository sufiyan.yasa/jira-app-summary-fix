package main

import (
	"encoding/csv"
	"io"
	"jira-summary-fix/lib"
	"net/url"
	"os"
	"regexp"
	"strings"
)

func main() {
	// Open the file
	validEntries := readAndFilter("all.csv")
	lib.SaveToCSV("step1_out.csv", validEntries)
}

func readAndFilter(filename string) [][]string {
	output := [][]string{}
	csvfile, err := os.Open(filename)
	lib.CheckError("Couldn't open the csv file", err)
	r := csv.NewReader(csvfile)

	wrongSummaryRegex := regexp.MustCompile(`^APP\s?:\s?(A|I)\s?:`)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		lib.CheckError("", err)
		summary, err := url.QueryUnescape(record[1])
		if wrongSummaryRegex.MatchString(summary) {
			if !strings.HasPrefix(summary, "APP: I:") && !strings.HasPrefix(summary, "APP: A:") {
				output = append(output, []string{record[0], summary})
			}
		}
	}
	return output

}
