package lib

import (
	"encoding/csv"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Cred struct {
		Email string `yaml:"email"`
		Token string `yaml:"token"`
	} `yaml:"cred"`
}

func CheckError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func SaveToCSV(filename string, entries [][]string) {
	file, err := os.Create(filename)
	CheckError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range entries {
		err := writer.Write(value)
		CheckError("Cannot write to file", err)
	}
}

func GetConfig() Config {
	f, err := os.Open("config.yml")
	CheckError("Config error %v", err)
	defer f.Close()

	var cfg Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	CheckError("Config decode error %v", err)
	return cfg
}
