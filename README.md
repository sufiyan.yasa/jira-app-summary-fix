# Jira summary fix

3 simple scripts to fix naming conventions to create APP tickets on JIRA.

Recommended format to use: `APP: <OS>: Title`

## How to use these script:

1. Prepare all the Jira tickets in a csv file called all.csv
2. Edit config.yml with your credentials.
3. run `go run step1_filter_app.go`. Briefly check validity of data on step1_out.csv
4. run `go run step2_replace_app.go`. Briefly check validity of data on step2_out.csv
5. run `go run step3_commit_app.go`

Script `step1_filter_app.go` and `step2_replace_app.go` produces an output file for backup (just incase something breaks)

## Creating an all.csv
A csv file containing `ticket`, `summary`. Please refer to example in repo.

The easiest way to create such a file is to run a JQL to filter for your project.
https://setelnow.atlassian.net/browse/FL-581?jql=project%3DFL%20order%20by%20created%20DESC

And export the results on Google Sheet or excel.

